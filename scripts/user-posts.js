const urlString = window.location.search;

let pageUserId = getUserId(urlString);

function getUserId(urlString){
    let numStr = urlString.replace(/[^0-9]/g,'');
    return numStr;
}

const xhr = new XMLHttpRequest();
xhr.open("GET", `http://13.90.251.61/users/${pageUserId}/posts`);
xhr.onreadystatechange = function(){
    if(xhr.readyState == 4){
        if(xhr.status == 200){
            const posts = JSON.parse(xhr.responseText);
            createPostContainers(posts);
        } else{
            console.log("something went wrong with your request");
        }
        
    }
}
xhr.send();

function createPostContainers(posts){

    for(let i = 0; i < posts.length; i++){

        let newW3Container = document.createElement("div");
        newW3Container.className = "w3-container ";
        newW3Container.className += "w3-hover-black ";
        newW3Container.className += randomBarColor();
        newW3Container.innerHTML = `<a href=post-details.html?postId=${posts[i].postId}>` + posts[i].title + "</a>";

        document.getElementById("post-container").insertBefore(newW3Container, document.getElementById("post-container").childNodes[0]);
    }

    usersPostTitle(posts);

}

function randomBarColor(){
    const flatColors = [
                        "w3-flat-turquoise", 
                        "w3-flat-green-sea", 
                        "w3-flat-emerald", 
                        "w3-flat-nephritis",
                        "w3-flat-peter-river",
                        "w3-flat-belize-hole",
                        "w3-flat-amethyst",
                        "w3-flat-wisteria",        
                        "w3-flat-orange",
                        "w3-flat-sun-flower",
                        "w3-flat-carrot",
                        "w3-flat-pumpkin",
                        "w3-flat-alizarin",
                        "w3-flat-pomegranate"
                    ];

    const vividColors = [
        "w3-vivid-pink",
        "w3-vivid-red",
        "w3-vivid-orange",
        "w3-vivid-yellow",
        "w3-vivid-green",
        "w3-vivid-blue",
        "w3-vivid-purple",
        "w3-vivid-yellowish-pink",
        "w3-vivid-reddish-orange",
        "w3-vivid-orange-yellow",
        "w3-vivid-greenish-yellow",
        "w3-vivid-yellow-green",
        "w3-vivid-yellowish-green",
        "w3-vivid-bluish-green",
        "w3-vivid-greenish-blue",
        "w3-vivid-purplish-blue",
        "w3-vivid-reddish-purple",
        "w3-vivid-purplish-red"
    ];

    const w3Colors = [
        "w3-red",
        "w3-pink",
        "w3-purple",
        "w3-deep-purple",
        "w3-indigo",
        "w3-blue",
        "w3-teal",
        "w3-green",
        "w3-deep-orange",
        "w3-blue-grey"
    ]
    let color = flatColors[Math.floor(Math.random() * flatColors.length)];
    return color;
}



function usersPostTitle(posts){
    document.getElementById("post-authored-by").innerHTML = "Posts by: " + posts[0].user.username;
}