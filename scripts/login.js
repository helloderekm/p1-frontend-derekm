document.getElementById("login-form").addEventListener("submit", attemptLogin);

function attemptLogin(event){
    event.preventDefault();
    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;
    ajaxLogin(username, password, successfulLogin, loginFailed);
}

function successfulLogin(xhr){
    console.log("success");
    const authToken = xhr.getResponseHeader("Authorization");
    const userId = xhr.getResponseHeader("User-Id");
    sessionStorage.setItem("token", authToken);
    sessionStorage.setItem("User-Id", userId);
    window.location.href = "./index.html";
}

function loginFailed(xhr){
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerHTML = "please check your credentials and try again.";
}