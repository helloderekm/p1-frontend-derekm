function sendAjaxRequest(method, url, body, successCallback, failureCallback, authToken, userId){

    const xhr = new XMLHttpRequest();
    xhr.open(method, url);

    if(authToken){
        xhr.setRequestHeader("Authorization", authToken);
    }

    if(userId){
        xhr.setRequestHeader("User-Id", userId);
    }

    xhr.onreadystatechange = function(){
    
        if(xhr.readyState == 4){
            if(xhr.status > 199 && xhr.status < 300){
                successCallback(xhr);
            } else{
                failureCallback(xhr);
            }

        }
    }

    if(body){
        xhr.send(body);
    } else{
        xhr.send();
    }

}

function sendAjaxPost(url, body, successCallback, failureCallback, authToken, userId){
    sendAjaxRequest("POST", url, body, successCallback, failureCallback, authToken, userId);
}

function ajaxLogin(username, password, successCallback, failureCallback){
    const payload = `username=${username}&password=${password}`;
    sendAjaxPost("http://13.90.251.61/login", payload, successCallback, failureCallback);
}

function ajaxRegister(username, password, email, successCallback, failureCallback){
    const payload = `username=${username}&password=${password}&email=${email}`;
    sendAjaxPost("http://13.90.251.61/register", payload, successCallback, failureCallback);
}

function ajaxCreatePost(post, successCallback, failureCallback){
    const postJson = JSON.stringify(post);
    const auth = sessionStorage.getItem("token");
    const userId = sessionStorage.getItem("User-Id");
    sendAjaxPost("http://13.90.251.61/posts", postJson, successCallback, failureCallback, auth, userId);
}

function ajaxCreateUser(user, successCallback, failureCallback){
    const userJson = JSON.stringify(user);
    sendAjaxPost("http://13.90.251.61/register", userJson, successCallback, failureCallback);
}

function ajaxEditPost(updatedPost, postId, successCallback, failureCallback){
    const updatePostJson = JSON.stringify(updatedPost);
    const auth = sessionStorage.getItem("token");
    const userId = sessionStorage.getItem("User-Id");
    sendAjaxRequest("PUT", `http://13.90.251.61/posts/${postId}`, updatePostJson, successCallback, failureCallback, auth, userId);
}

function ajaxDeletePost(postId, successCallback, failureCallback){
    const auth = sessionStorage.getItem("token");
    const userId = sessionStorage.getItem("User-Id");
    sendAjaxRequest("DELETE", `http://13.90.251.61/posts/${postId}`, null, successCallback, failureCallback, auth, userId);
}

function ajaxCreateComment(comment, successCallback, failureCallback){
    const commentJson = JSON.stringify(comment);
    const auth = sessionStorage.getItem("token");
    const userId = sessionStorage.getItem("User-Id");
    sendAjaxRequest("POST", "http://13.90.251.61/comments", commentJson, successCallback, failureCallback, auth, userId);
}