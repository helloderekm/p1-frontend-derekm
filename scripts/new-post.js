document.getElementById("new-post-form").addEventListener("submit", addNewPost);

function addNewPost(e){
    e.preventDefault();
    const title = document.getElementById("inputTitle").value;
    const body = document.getElementById("inputMessage").value;
    const newPost = {"title": title, "postBody":body};
    ajaxCreatePost(newPost, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerHTML = "new post successfully created";
}

function indicateFailure(){
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerHTML = "post could not be created";
}