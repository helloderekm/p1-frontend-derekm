const xhr = new XMLHttpRequest();
xhr.open("GET", "http://13.90.251.61/posts");
xhr.onreadystatechange = function(){
    if(xhr.readyState == 4){
        if(xhr.status == 200){
            const posts = JSON.parse(xhr.responseText);
            //createPostBars(posts);
            createPostContainers(posts);
        } else{
            console.log("something went wrong with your request");
        }
        
    }
}
xhr.send();

function savePostTitles(posts){
    for(let i = 0; i < posts.length; i++){
        postTitles[i] = posts[i].title;
        console.log(postTitles[i]);
    }
}


function createPostBars(posts){
    for(let i = 0; i < posts.length; i++){

        let newW3Bar = document.createElement("div");
        newW3Bar.className = "w3-bar ";
        newW3Bar.className += "w3-hover-white ";
        //newW3Bar.className += "post-bar ";
        newW3Bar.className += randomBarColor();

        let newBarItem = document.createElement("div");
        newBarItem.id = "post-bar-item";
        newBarItem.className = "w3-bar-item ";
        newBarItem.innerHTML = `<a href=http://13.90.251.61/posts/${posts[i].postId}>` + posts[i].title + "</a>";
        newW3Bar.appendChild(newBarItem);

        document.getElementById("post-container").insertBefore(newW3Bar, document.getElementById("post-container").childNodes[0]);

        //document.getElementById("post-container").appendChild(newW3Bar);

    }
}

function randomBarColor(){
    const flatColors = [
                        //"w3-flat-turquoise", 
                        "w3-flat-green-sea", 
                        "w3-flat-emerald", 
                        "w3-flat-nephritis",
                        "w3-flat-peter-river",
                        "w3-flat-belize-hole",
                        "w3-flat-amethyst",
                        "w3-flat-wisteria",        
                        "w3-flat-orange",
                        "w3-flat-sun-flower",
                        "w3-flat-carrot",
                        "w3-flat-pumpkin",
                        "w3-flat-alizarin",
                        "w3-flat-pomegranate"
                    ];

    const vividColors = [
        "w3-vivid-pink",
        "w3-vivid-red",
        "w3-vivid-orange",
        "w3-vivid-yellow",
        "w3-vivid-green",
        "w3-vivid-blue",
        "w3-vivid-purple",
        "w3-vivid-yellowish-pink",
        "w3-vivid-reddish-orange",
        "w3-vivid-orange-yellow",
        "w3-vivid-greenish-yellow",
        "w3-vivid-yellow-green",
        "w3-vivid-yellowish-green",
        "w3-vivid-bluish-green",
        "w3-vivid-greenish-blue",
        "w3-vivid-purplish-blue",
        "w3-vivid-reddish-purple",
        "w3-vivid-purplish-red"
    ];

    const w3Colors = [
        "w3-red",
        "w3-pink",
        "w3-purple",
        "w3-deep-purple",
        "w3-indigo",
        "w3-blue",
        "w3-teal",
        "w3-green",
        "w3-deep-orange",
        "w3-blue-grey"
    ]
    let color = flatColors[Math.floor(Math.random() * flatColors.length)];
    return color;
}

function createPostContainers(posts){

    for(let i = 0; i < posts.length; i++){

        let newW3Container = document.createElement("div");
        newW3Container.className = "w3-container ";
        newW3Container.className += "w3-hover-black ";
        newW3Container.className += randomBarColor();
        newW3Container.innerHTML = `<a href=post-details.html?postId=${posts[i].postId}>` + posts[i].title + "</a>";

        document.getElementById("post-container").insertBefore(newW3Container, document.getElementById("post-container").childNodes[0]);
    }

}




// let postTitles = getPostTitles(posts);

// function getPostTitles(posts){
//     for(let i = 0; i < posts.length; i++){
//         return posts[i].title;
//     }
// }

// console.log(postTitles);