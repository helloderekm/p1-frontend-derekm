const token = sessionStorage.getItem("token");
const userId = sessionStorage.getItem("User-Id");



if(token){
    document.getElementById("login-nav").style.display = "none";
    document.getElementById("register-nav").style.display = "none";
    document.getElementById("logout-nav").style.display = "initial";
    document.getElementById("welcome-nav").style.display = "initial";
    document.getElementById("create-post-button").style.display ="initial";
    

    const xhr = new XMLHttpRequest();
    xhr.open("GET", `http://13.90.251.61/users/${userId}`);
    xhr.setRequestHeader("Authorization",token);
    xhr.setRequestHeader("User-Id", userId);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                const user = JSON.parse(xhr.responseText);
                helloUser(user);
            } else{
                console.log("something went wrong with your request");
            }
            
        }
    }
    xhr.send();
}

document.getElementById("logout-nav").addEventListener("click", logout);

function logout(){
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("User-Id");

    document.getElementById("login-nav").style.display = "initial";
    document.getElementById("register-nav").style.display = "initial";
    document.getElementById("logout-nav").style.display = "none";
    document.getElementById("create-post-button").style.display ="none";
    document.getElementById("welcome-nav").style.display = "none";
}

function helloUser(user){
    document.getElementById("welcome-nav").innerHTML = "Hello, " + user.username;
}
